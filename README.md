# mountains-os-docker

#### Docker 环境搭建

- 安装docker-compose
> 依赖包: py-pip, python-dev, libffi-dev, openssl-dev, gcc, libc-dev, and make.
```bash
下载安装
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
或直接拷贝使用
$ sudo mv ./docker-compose-Linux-x86_64 /usr/local/bin/docker-compose
添加执行权限
$ sudo chmod +x /usr/local/bin/docker-compose
```
---
- 启动开发环境
```bash
$ docker-compose up -d
``` 
---
- 卸载docker-compose
```bash
$ sudo rm /usr/local/bin/docker-compose
```