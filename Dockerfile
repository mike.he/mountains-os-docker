FROM ubuntu:18.04

WORKDIR /data

ADD ./docker/apt/sources.list /etc/apt/sources.list

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
    libssl-dev \
    libmcrypt-dev \
    libcurl4-openssl-dev \
    libjpeg-turbo8-dev \
    libpng-dev \
    libzip-dev \
    zlib1g-dev \
    m4 \
    autoconf \
    curl \
    unzip \
    && apt-get clean
RUN apt-get install -y --fix-missing \
    make vim wget git \
    gcc \
    libpcre3-dev \
    libxml2-dev \
    nodejs \
    && apt-get clean
RUN apt-get install -y --fix-missing \
    libssl1.0-dev \
    nodejs-dev \
    node-gyp \
    && apt-get clean
RUN wget http://nginx.org/download/nginx-1.9.9.tar.gz \
    && tar -zxvf nginx-1.9.9.tar.gz \
    && cd nginx-1.9.9 \
    && ./configure --prefix=/usr/share/nginx \
    --sbin-path=/usr/sbin/nginx \
    --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --pid-path=/run/nginx.pid \
    --lock-path=/var/lock/nginx.lock \
    --user=www-data \
    --group=www-data \
    --build=Ubuntu \
    --with-pcre-jit \
    --with-file-aio \
    --with-threads \
    --with-http_addition_module \
    --with-http_auth_request_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_mp4_module \
    --with-http_random_index_module \
    --with-http_realip_module \
    --with-http_slice_module \
    --with-http_sub_module \
    --with-http_stub_status_module \
    --with-http_v2_module \
    --with-http_secure_link_module \
    --with-mail \
    --with-stream \
    --with-debug \
    && sed -i 's/CFLAGS =  -pipe  -O -W -Wall -Wpointer-arith -Wno-unused -Werror -g/CFLAGS =  -pipe  -O -W -Wall -Wpointer-arith -Wno-unused -g/' objs/Makefile \
    && make && make install \
    && mkdir /etc/nginx/sites-available \
    && mkdir /etc/nginx/sites-enabled \
    && mkdir /etc/nginx/sites-conf.d \
    && cd .. \
    && rm -rf nginx-1.9.9*
RUN wget https://www.php.net/distributions/php-7.3.6.tar.gz \
    && tar -zxvf php-7.3.6.tar.gz \
    && cd php-7.3.6 \
    && ./configure \
    --prefix=/usr/local/php7 \
    --exec-prefix=/usr/local/php7 \
    --bindir=/usr/local/php7/bin \
    --sbindir=/usr/local/php7/sbin \
    --includedir=/usr/local/php7/include \
    --libdir=/usr/local/php7/lib/php \
    --mandir=/usr/local/php7/php/man \
    --with-config-file-path=/usr/local/php7/etc \
    --with-mysql-sock=/var/run/mysql/mysql.sock \
    --with-mhash \
    --with-openssl \
    --with-mysqli=shared,mysqlnd \
    --with-pdo-mysql=shared,mysqlnd \
    --with-gd \
    --with-iconv \
    --with-zlib \
    --enable-zip \
    --enable-inline-optimization \
    --enable-debug \
    --enable-rpath \
    --enable-shared \
    --enable-xml \
    --enable-bcmath \
    --enable-shmop \
    --enable-sysvsem \
    --enable-mbregex \
    --enable-mbstring \
    --enable-ftp \
    --enable-pcntl \
    --enable-sockets \
    --enable-soap \
    --without-pear \
    --with-gettext \
    --enable-session \
    --enable-opcache \
    --enable-fpm \
    --with-fpm-user=root \
    --with-fpm-group=root \
    --enable-fileinfo \
    && make && make install \
    && mv /usr/local/php7/etc/php-fpm.conf.default /usr/local/php7/etc/php-fpm.conf \
    && mv /usr/local/php7/etc/php-fpm.d/www.conf.default /usr/local/php7/etc/php-fpm.d/www.conf \
    && groupadd nobody \
    && echo "export PATH=\"\$PATH:/usr/local/php7/bin:/usr/local/php7/sbin\"" >> /root/.bashrc \
    && ln -s /usr/local/php7/bin/php /usr/local/bin/php \
    && cd .. \
    && rm -rf php-7.3.6*
RUN wget https://dl.laravel-china.org/composer.phar -O /usr/local/bin/composer \
    && chmod a+x /usr/local/bin/composer
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install yarn \
    && apt-get clean

EXPOSE 80
EXPOSE 2346

CMD ["top", "-b"]
